	niosf_tx_tse_125mhz u0 (
		.clk_clk                              (<connected-to-clk_clk>),                              //                              clk.clk
		.reset_reset_n                        (<connected-to-reset_reset_n>),                        //                            reset.reset_n
		.tse_mac_mdio_connection_mdc          (<connected-to-tse_mac_mdio_connection_mdc>),          //          tse_mac_mdio_connection.mdc
		.tse_mac_mdio_connection_mdio_in      (<connected-to-tse_mac_mdio_connection_mdio_in>),      //                                 .mdio_in
		.tse_mac_mdio_connection_mdio_out     (<connected-to-tse_mac_mdio_connection_mdio_out>),     //                                 .mdio_out
		.tse_mac_mdio_connection_mdio_oen     (<connected-to-tse_mac_mdio_connection_mdio_oen>),     //                                 .mdio_oen
		.tse_pcs_ref_clk_clock_connection_clk (<connected-to-tse_pcs_ref_clk_clock_connection_clk>), // tse_pcs_ref_clk_clock_connection.clk
		.tse_serial_connection_txp            (<connected-to-tse_serial_connection_txp>),            //            tse_serial_connection.txp
		.tse_serial_connection_rxp            (<connected-to-tse_serial_connection_rxp>)             //                                 .rxp
	);

