module generator
(
    // clock interface
    input           csi_clock_clk,
    input           csi_clock_reset,
    
    // slave interface
    input           avs_s0_write,
    input           avs_s0_read,
    input   [1:0]   avs_s0_address,
    input   [3:0]   avs_s0_byteenable,
    input   [31:0]  avs_s0_writedata,
    output  [31:0]  avs_s0_readdata,
    
    // source interface
    output          aso_src0_valid,
    input           aso_src0_ready,
    output  [11:0]  aso_src0_data,
    output          aso_src0_channel
);

localparam [1:0] IDLE_STATE = 2'h0;
localparam [1:0] DATA_STATE = 2'h1;


reg             go_bit;
reg             running_bit;
reg     [11:0]  next_value;
reg     [1:0]   state;
reg             channel;
reg             valid;

//
// slave read mux
//
assign avs_s0_readdata = (avs_s0_address == 2'h0) ? ({{30{1'b0}}, running_bit, go_bit}) : (32'b0);
                            
//
// slave write demux
//
always @ (posedge csi_clock_clk or posedge csi_clock_reset)
begin
    if(csi_clock_reset)
    begin
        go_bit                  <= 0;
        
    end
    else
    begin
        if(avs_s0_write)
        begin
            if (avs_s0_address == 2'h0)
                begin
                   if (avs_s0_byteenable[0] == 1'b1)
                        go_bit  <= avs_s0_writedata[0];
                end
        end
        else
        begin
            go_bit  <= go_bit;
        end
    end
end

//
// running_bit state machine
//

always @ (posedge csi_clock_clk or posedge csi_clock_reset)
begin
    if(csi_clock_reset)
    begin
        running_bit <= 0;
    end
    else
    begin
        if(go_bit)
        begin
            running_bit <= 1;
        end
        else if(running_bit & !go_bit & aso_src0_valid & aso_src0_ready)
        begin
            running_bit <= 0;
        end
    end
end

//
// next_value state machine
//

always @ (posedge csi_clock_clk or posedge csi_clock_reset)
begin
    if(csi_clock_reset)
    begin
        next_value  <= 0;
    end
    else
    begin
        if(aso_src0_ready)
        begin
            next_value <= next_value + 1;
            valid <= 1;  
        end
        else
        begin
            valid <= 0; 
        end
    end
end

//
// source interface control
//
// these are combinatorial control equations for our source interface
//
assign aso_src0_valid           =   go_bit;

assign aso_src0_data            =   next_value;

assign aso_src0_channel         =   channel;

always @ (posedge csi_clock_clk or posedge csi_clock_reset)
begin
    if(csi_clock_reset)
    begin
        channel  <= 0;
    end
    else
    begin
        if (aso_src0_ready & go_bit)
        begin
            if(channel == 1'b0)
            begin
                channel <= 1'b1;
            end
            else
            begin
                channel <= 1'b0; 
            end
        end
        else
        begin
            channel <= channel;    
        end
    end
end

//
// source state machine
//
// this state machine provides synchronous sequencing for the control of the
// source interface
//
always @ (posedge csi_clock_clk or posedge csi_clock_reset)
begin
    if(csi_clock_reset)
    begin
        state <= IDLE_STATE;
    end
    else
    begin
        case(state)
            IDLE_STATE:
            begin
                if(running_bit)
                begin
                    state <= DATA_STATE;
                end
                else
                begin
                    state <= IDLE_STATE;    
                end
            end
            DATA_STATE:
            begin
                if(!go_bit || !running_bit)
                begin
                    state <= IDLE_STATE;
                end
                else
                begin
                    state <= DATA_STATE;
                end
            end
        endcase
    end
end
endmodule