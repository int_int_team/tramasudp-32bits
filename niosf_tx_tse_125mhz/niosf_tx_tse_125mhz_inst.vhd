	component niosf_tx_tse_125mhz is
		port (
			clk_clk                              : in  std_logic := 'X'; -- clk
			reset_reset_n                        : in  std_logic := 'X'; -- reset_n
			tse_mac_mdio_connection_mdc          : out std_logic;        -- mdc
			tse_mac_mdio_connection_mdio_in      : in  std_logic := 'X'; -- mdio_in
			tse_mac_mdio_connection_mdio_out     : out std_logic;        -- mdio_out
			tse_mac_mdio_connection_mdio_oen     : out std_logic;        -- mdio_oen
			tse_pcs_ref_clk_clock_connection_clk : in  std_logic := 'X'; -- clk
			tse_serial_connection_txp            : out std_logic;        -- txp
			tse_serial_connection_rxp            : in  std_logic := 'X'  -- rxp
		);
	end component niosf_tx_tse_125mhz;

	u0 : component niosf_tx_tse_125mhz
		port map (
			clk_clk                              => CONNECTED_TO_clk_clk,                              --                              clk.clk
			reset_reset_n                        => CONNECTED_TO_reset_reset_n,                        --                            reset.reset_n
			tse_mac_mdio_connection_mdc          => CONNECTED_TO_tse_mac_mdio_connection_mdc,          --          tse_mac_mdio_connection.mdc
			tse_mac_mdio_connection_mdio_in      => CONNECTED_TO_tse_mac_mdio_connection_mdio_in,      --                                 .mdio_in
			tse_mac_mdio_connection_mdio_out     => CONNECTED_TO_tse_mac_mdio_connection_mdio_out,     --                                 .mdio_out
			tse_mac_mdio_connection_mdio_oen     => CONNECTED_TO_tse_mac_mdio_connection_mdio_oen,     --                                 .mdio_oen
			tse_pcs_ref_clk_clock_connection_clk => CONNECTED_TO_tse_pcs_ref_clk_clock_connection_clk, -- tse_pcs_ref_clk_clock_connection.clk
			tse_serial_connection_txp            => CONNECTED_TO_tse_serial_connection_txp,            --            tse_serial_connection.txp
			tse_serial_connection_rxp            => CONNECTED_TO_tse_serial_connection_rxp             --                                 .rxp
		);

