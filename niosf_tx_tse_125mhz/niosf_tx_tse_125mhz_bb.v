
module niosf_tx_tse_125mhz (
	clk_clk,
	reset_reset_n,
	tse_mac_mdio_connection_mdc,
	tse_mac_mdio_connection_mdio_in,
	tse_mac_mdio_connection_mdio_out,
	tse_mac_mdio_connection_mdio_oen,
	tse_pcs_ref_clk_clock_connection_clk,
	tse_serial_connection_txp,
	tse_serial_connection_rxp);	

	input		clk_clk;
	input		reset_reset_n;
	output		tse_mac_mdio_connection_mdc;
	input		tse_mac_mdio_connection_mdio_in;
	output		tse_mac_mdio_connection_mdio_out;
	output		tse_mac_mdio_connection_mdio_oen;
	input		tse_pcs_ref_clk_clock_connection_clk;
	output		tse_serial_connection_txp;
	input		tse_serial_connection_rxp;
endmodule
