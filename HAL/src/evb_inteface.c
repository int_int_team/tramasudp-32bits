#include "evb_interface.h"
#include "evb_interface_regs.h"

//
// evb_interface utility routines
//

int start_evb_interface(void *base) {
    
    alt_u32 current_csr;

    // is the packet EVB already running?
    current_csr = EVB_RD_CSR(base);
    if(current_csr & EVB_CSR_GO_BIT_MASK) {
        return 1;
    }
    
    // and set the go bit
    EVB_WR_CSR(base, EVB_CSR_GO_BIT_MASK);
    
    return 0;
}

int stop_evb_inteface(void *base) {
    
    // is the packet EVB already stopped?
    if(!(EVB_RD_CSR(base) & EVB_CSR_GO_BIT_MASK)) {
        return 1;
    }

    // clear the go bit
    EVB_WR_CSR(base, 0);
    
    return 0;
}


