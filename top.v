module top (

input csi_clock_clk_sig,
input csi_clock_reset_sig,

//Inputs Generator
input avs_s0_write_gen,
input avs_s0_read_gen,
input [1:0]avs_s0_address_gen,
input [3:0]avs_s0_byteenable_gen,
input [31:0]avs_s0_writedata_gen,
input [31:0]avs_s0_readdata_gen,

//Inputs Packets
input avs_s0_write_pac,
input avs_s0_read_pac,
input [1:0]avs_s0_address_pac,
input [3:0]avs_s0_byteenable_pac,
input [31:0]avs_s0_writedata_pac,
input [31:0]avs_s0_readdata_pac,


//Inputs UDP Inserter
input avs_s0_write_udp,
input avs_s0_read_udp,
input [3:0]avs_s0_address_udp,
input [3:0]avs_s0_byteenable_udp,
input [31:0]avs_s0_writedata_udp,
input [31:0]avs_s0_readdata_udp,


input aso_src0_ready_tse,

//Output Data UDP
output [31:0] aso_src0_data_udp,
output aso_src0_valid_udp,
output aso_src0_startofpacket_udp,
output aso_src0_endofpacket_udp
);

//wire Generator
wire aso_src0_valid_gen, aso_src0_ready_pac,aso_src0_channel_gen;
wire [11:0]aso_src0_data_gen;

//wire Packet
wire aso_src0_valid_pac, asi_snk0_ready_udp;
wire [31:0]aso_src0_data_pac;
wire aso_src0_startofpacket_pac, aso_src0_endofpacket_pac;

generator generator_inst
(
	.csi_clock_clk(csi_clock_clk_sig) ,	// input  csi_clock_clk_sig
	.csi_clock_reset(csi_clock_reset_sig) ,	// input  csi_clock_reset_sig
	
	.avs_s0_write(avs_s0_write_gen) ,	// input  avs_s0_write_sig
	.avs_s0_read(avs_s0_read_gen) ,	// input  avs_s0_read_sig
	.avs_s0_address(avs_s0_address_gen) ,	// input [1:0] avs_s0_address_sig
	.avs_s0_byteenable(avs_s0_byteenable_gen) ,	// input [3:0] avs_s0_byteenable_sig
	.avs_s0_writedata(avs_s0_writedata_gen) ,	// input [31:0] avs_s0_writedata_sig
	.avs_s0_readdata(avs_s0_readdata_gen) ,	// output [31:0] avs_s0_readdata_sig
	
	.aso_src0_valid(aso_src0_valid_gen) ,	// output  aso_src0_valid_sig
	.aso_src0_ready(asi_snk0_ready_pac) ,	// input  aso_src0_ready_sig
	.aso_src0_data(aso_src0_data_gen), 	// output [11:0] aso_src0_data_sig
	.aso_src0_channel(aso_src0_channel_gen)
);

packet packet_inst
(
	.csi_clock_clk(csi_clock_clk_sig) ,	// input  csi_clock_clk_sig
	.csi_clock_reset(csi_clock_reset_sig) ,	// input  csi_clock_reset_sig
	
	.avs_s0_write(avs_s0_write_pac) ,	// input  avs_s0_write_sig
	.avs_s0_read(avs_s0_read_pac) ,	// input  avs_s0_read_sig
	.avs_s0_address(avs_s0_address_pac) ,	// input  avs_s0_address_sig
	.avs_s0_byteenable(avs_s0_byteenable_pac) ,	// input [3:0] avs_s0_byteenable_sig
	.avs_s0_writedata(avs_s0_writedata_pac) ,	// input [31:0] avs_s0_writedata_sig
	.avs_s0_readdata(avs_s0_readdata_pac) ,	// output [31:0] avs_s0_readdata_sig
	
	.asi_snk0_valid(aso_src0_valid_gen) ,	// input  asi_snk0_valid_sig
	.asi_snk0_ready(asi_snk0_ready_pac) ,	// output  asi_snk0_ready_sig
	.asi_snk0_data(aso_src0_data_gen) ,	// input [11:0] asi_snk0_data_sig
	.asi_snk0_channel(aso_src0_channel_gen) ,	// input  asi_snk0_channel_sig

	.aso_src0_valid(aso_src0_valid_pac) ,	// output  aso_src0_valid_sig
	.aso_src0_ready(asi_snk0_ready_udp) ,	// input  aso_src0_ready_sig
	.aso_src0_data(aso_src0_data_pac) ,	// output [31:0] aso_src0_data_sig
	.aso_src0_startofpacket(aso_src0_startofpacket_pac) ,	// output  aso_src0_startofpacket_sig
	.aso_src0_endofpacket(aso_src0_endofpacket_pac) 	// output  aso_src0_endofpacket_sig
);

udp_payload_inserter udp_payload_inserter_inst
(
	.csi_clock_clk(csi_clock_clk_sig) ,	// input  csi_clock_clk_sig
	.csi_clock_reset(csi_clock_reset_sig) ,	// input  csi_clock_reset_sig
	
	.avs_s0_write(avs_s0_write_udp) ,	// input  avs_s0_write_sig
	.avs_s0_read(avs_s0_read_udp) ,	// input  avs_s0_read_sig
	.avs_s0_address(avs_s0_address_udp) ,	// input [3:0] avs_s0_address_sig
	.avs_s0_byteenable(avs_s0_byteenable_udp) ,	// input [3:0] avs_s0_byteenable_sig
	.avs_s0_writedata(avs_s0_writedata_udp) ,	// input [31:0] avs_s0_writedata_sig
	.avs_s0_readdata(avs_s0_readdata_udp) ,	// output [31:0] avs_s0_readdata_sig
	
	.asi_snk0_valid(aso_src0_valid_pac) ,	// input  asi_snk0_valid_sig
	.asi_snk0_ready(asi_snk0_ready_udp) ,	// output  asi_snk0_ready_sig
	.asi_snk0_data(aso_src0_data_pac) ,	// input [31:0] asi_snk0_data_sig
	.asi_snk0_startofpacket(aso_src0_startofpacket_pac) ,	// input  asi_snk0_startofpacket_sig
	.asi_snk0_endofpacket(aso_src0_endofpacket_pac), 	// input  asi_snk0_endofpacket_sig

	.aso_src0_valid(aso_src0_valid_udp) ,	// output  aso_src0_valid_sig
	.aso_src0_ready(aso_src0_ready_tse) ,	// input  aso_src0_ready_sig
	.aso_src0_data(aso_src0_data_udp) ,	// output [31:0] aso_src0_data_sig
	//.aso_src0_empty(aso_src0_empty_udp) ,	// output [1:0] aso_src0_empty_sig
	.aso_src0_startofpacket(aso_src0_startofpacket_udp) ,	// output  aso_src0_startofpacket_sig
	.aso_src0_endofpacket(aso_src0_endofpacket_udp) 	// output  aso_src0_endofpacket_sig
	//.aso_src0_error(aso_src0_error_udp)	// output  aso_src0_error_sig
);

endmodule 
