module niosf_tse(
	// Clock
	input       OSC_50_BANK2,
	
	// KEY
	input  [ 0 :0] BUTTON,
		
	// Ethernet
	input  [0:0] ETH_RX_p,
	output [0:0] ETH_TX_p,	
	output       ETH_RST_n,
	output [0:0] ETH_MDC,
	inout  [0:0] ETH_MDIO
);

	wire sys_clk,core_reset_n;
	wire mdc, mdio_in, mdio_oen, mdio_out;
	
	assign mdio_in  = ETH_MDIO;
	assign ETH_MDC  = mdc;
	assign ETH_MDIO = mdio_oen ? 1'bz : mdio_out;
	
	assign ETH_RST_n = core_reset_n;
		
	pll pll_inst(
		.areset	(~BUTTON[0]),
		.inclk0	(OSC_50_BANK2),
		.c0		(sys_clk),
		.locked	(core_reset_n)
	);
		
   niosf_tx_tse_125mhz niosf_tx_tse_125mhz_inst(
        .clk_clk                              (sys_clk),       //                              clk.clk
        .reset_reset_n                        (core_reset_n),  //                            reset.reset_n
        .tse_serial_connection_txp            (ETH_TX_p),   //            tse_serial_connection.txp
        .tse_serial_connection_rxp            (ETH_RX_p),   //                                 .rxp
        .tse_pcs_ref_clk_clock_connection_clk (sys_clk),			// tse_pcs_ref_clk_clock_connection.clk
        .tse_mac_mdio_connection_mdc          (mdc),          	//          tse_mac_mdio_connection.mdc
        .tse_mac_mdio_connection_mdio_in      (mdio_in),      	//                                 .mdio_in
        .tse_mac_mdio_connection_mdio_out     (mdio_out),     	//                                 .mdio_out
        .tse_mac_mdio_connection_mdio_oen     (mdio_oen)      	//                                 .mdio_oen  
 );		
	
endmodule 
