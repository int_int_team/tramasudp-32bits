#ifndef GENERATOR_REGS_H
#define GENERATOR_REGS_H

#include "io.h"

// GENERATOR_REGS accessor macros

#define GENERATOR_RD_CSR(base)                  IORD(base, 0)
#define GENERATOR_WR_CSR(base, data)            IOWR(base, 0, data)

#define GENERATOR_CSR_GO_BIT_MASK               (0x01)
#define GENERATOR_CSR_GO_BIT_OFST               (0)
#define GENERATOR_CSR_RUNNING_BIT_MASK          (0x02)
#define GENERATOR_CSR_RUNNING_BIT_OFST          (1)

#endif /*GENERATOR_REGS_H*/
