#include "generator.h"
#include "generator_regs.h"

//
// prbs_packet_generator utility routines
//

int start_packet_generator(void *base) {
    
    alt_u32 current_csr;

    // is the packet generator already running?
    current_csr = GENERATOR_RD_CSR(base);
    if(current_csr & GENERATOR_CSR_GO_BIT_MASK) {
        return 1;
    }
    if(current_csr & GENERATOR_CSR_RUNNING_BIT_MASK) {
        return 2;
    }
    
    // and set the go bit
    GENERATOR_WR_CSR(base, GENERATOR_CSR_GO_BIT_MASK);
    
    return 0;
}

int stop_packet_generator(void *base) {
    
    // is the packet generator already stopped?
    if(!(GENERATOR_RD_CSR(base) & GENERATOR_CSR_GO_BIT_MASK)) {
        return 1;
    }

    // clear the go bit
    GENERATOR_WR_CSR(base, 0);
    
    return 0;
}

int is_packet_generator_running(void *base) {
    
    // is the packet generator running?
    if((GENERATOR_RD_CSR(base) & GENERATOR_CSR_RUNNING_BIT_MASK)) {
        return 1;
    }

    return 0;
}

int wait_until_packet_generator_stops_running(void *base) {
    
    // wait until packet generator stops running?
    while(is_packet_generator_running(base));

    return 0;
}

int get_packet_generator_stats(void *base, PKT_GEN_STATS *stats) {
    
    stats->csr_state        = GENERATOR_RD_CSR(base);

    return 0;
}

