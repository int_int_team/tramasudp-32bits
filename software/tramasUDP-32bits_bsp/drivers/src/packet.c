#include "packet.h"
#include "packet_regs.h"

//
// packet utility routines
//

int start_packet(void *base, int payload) {
    
    alt_u32 current_csr;

    // is the packet evb already running?
    current_csr = PACKET_RD_CSR(base);
    if(current_csr & PACKET_CSR_GO_BIT_MASK) {
        return 1;
    }
    if(current_csr & PACKET_CSR_RUNNING_BIT_MASK) {
        return 2;
    }
    
    // clear the counter    
	CLEAR_PACKET_COUNTER(base);
    // write the parameter registers
	PACKET_BYTE_PAYLOAD(base, payload);
    // and set the go bit
	PACKET_WR_CSR(base, PACKET_CSR_GO_BIT_MASK);
    
    return 0;
}

int stop_packet(void *base) {
    
    // is the packet evb already stopped?
    if(!(PACKET_RD_CSR(base) & PACKET_CSR_GO_BIT_MASK)) {
        return 1;
    }

    // clear the go bit
    PACKET_WR_CSR(base, 0);
    
    return 0;
}

int is_packet_running(void *base) {
    
    // is the packet evb running?
    if((PACKET_RD_CSR(base) & PACKET_CSR_RUNNING_BIT_MASK)) {
        return 1;
    }

    return 0;
}

int wait_until_packet_stops_running(void *base) {
    
    // wait until packet evb stops running?
    while(is_packet_running(base));

    return 0;
}
