#include <system.h>
#include <priv/alt_legacy_irq.h>
#include <altera_eth_tse_regs.h>
#include <altera_avalon_tse_system_info.h>
#include <unistd.h>
#include <alt_types.h>
#include <stdio.h>
//#include "sys/alt_stdio.h"
#include <generator.h>
#include <generator_regs.h>
#include <packet.h>
#include <packet_regs.h>
#include <udp_payload_inserter.h>
#include <udp_payload_inserter_regs.h>


#define PAYLOAD_SIZE 400

int main(void)
{
	//alt_printf ("Hello Nios II - Tramas UDP OffLoad \n");
	np_tse_mac * tse = (np_tse_mac*) TSE_BASE;

	/*	External PHY Initialization using MDIO

		ACLARACION!!!
		If your MAC variation includes the PCS function, the PCS function is
		always device 0 and its configuration registers (PCS Configuration
		Register Space on page 5-17) occupy MDIO Space 0. You can use
		MDIO Space 1 to map to a PHY device.
	*/

	tse->MDIO_ADDR0=0x10;	//PCS - Direccion configurada en QSys
	tse->MDIO_ADDR1=0x00;	//Chip Marvell 88E1111



/*	PCS Configuration Register Initialization
		a. Set Auto Negotiation Link Timer
		//Set Link timer to 1.6ms for SGMII
		link_timer (address offset 0x12) = 0x0D40
		Link_timer (address offset 0x13) = 0x03*/
	tse->mdio0.reg12= 0x0D40;
	tse->mdio0.reg13= 0x03;

/*		b.Configure SGMII
		//Enable SGMII Interface and Enable SGMII Auto Negotiation
		//SGMII_ENA = 1, USE_SGMII_AN = 1
		if_mode = 0x0003*/

	tse->mdio0.reg14|=0x0003;

/*		c. Enable Auto Negotiation
		//Enable Auto Negotiation
		//AUTO_NEGOTIATION_ENA = 1, Bit 6,8,13 can be ignore
		PCS Control Register = 0x1140*/

	tse->mdio0.CONTROL|=PCS_CTL_an_enable;

/*		d. PCS Reset
		//PCS Software reset is recommended where there any configuration changed
		//RESET = 1
		PCS Control Register = 0x9140
		Wait PCS Control Register RESET bit is clear*/

	//Crossover
	//tse->mdio1.reg10|=0x0060;


	tse->mdio0.CONTROL|=PCS_CTL_sw_reset;
	while(tse->mdio0.CONTROL & PCS_CTL_sw_reset)
		;
/*	2. MAC Configuration Register Initialization
		a. Disable MAC Transmit and Receive DatapathDisable the MAC transmit and receive datapath
		before performing any changes to configuration.
		//Set TX_ENA and RX_ENA bit to 0 in Command Config Register
		Command_config Register = 0x00802220
		//Read the TX_ENA and RX_ENA bit is set 0 to ensure TX and RX path is disable
		Wait Command_config Register = 0x00802220
		COMMAND_CONFIG HW RESET=0*/
	//tse->COMMAND_CONFIG&=~(mmac_cc_RX_ENA_mask | mmac_cc_TX_ENA_mask);
	//while(tse->COMMAND_CONFIG & (mmac_cc_RX_ENA_mask | mmac_cc_TX_ENA_mask)) //Arreglar esto
		;
/*		c. MAC Address Configuration
		//MAC address is 00-1C-23-17-4A-CB
		mac_0 = 0x17231C00
		mac_1 = 0x0000CB4A*/

	//HARDCODEAR ESTO?
	tse->MAC_0=0x17231C00;
	tse->MAC_1=0x0000CB4A;

/*		e. Reset MAC
		Altera recommends that you perform a software reset when there is a change in the MAC speed or
		duplex. The MAC software reset bit self-clears when the software reset is complete.
		//Set SW_RESET bit to 1
		Command_config Register = 0x00802220
		Wait Command_config Register = 0x00800220*/
	tse->COMMAND_CONFIG|=mmac_cc_SW_RESET_mask;
	while(tse->COMMAND_CONFIG & mmac_cc_SW_RESET_mask)
		;

/*		f. Enable MAC Transmit and Receive Datapath
		//Set TX_ENA and RX_ENA to 1 in Command Config Register
		Command_config Register = 0x00800223
		//Read the TX_ENA and RX_ENA bit is set 1 to ensure TX and RX path is enable
		Wait Command_config Register = 0x00800223*/

	tse->COMMAND_CONFIG|=(mmac_cc_RX_ENA_mask | mmac_cc_TX_ENA_mask);
	while(!(tse->COMMAND_CONFIG & (mmac_cc_RX_ENA_mask | mmac_cc_TX_ENA_mask)))
		;

	//	Configracion cabeceras UDP
	UDP_INS_STATS *stats = 0;

	stats->mac_dst_hi = 0xffffffff;
	stats->mac_dst_lo = 0x0000ffff;
	stats->mac_src_hi = 0x17231C00;
	stats->mac_src_lo = 0x0000CB4A;
	stats->ip_dst	  = 3232236031; // 192.168.1.255 -> 3232236031 // 192.168.1.10 -> 3232235786
	stats->ip_src	  = 3232235928; // 192.168.1.152 -> 3232235928
	stats->udp_dst	  = 2020;
	stats->udp_src	  = 2020;

int i=5;
 // RX PATH:
 // prbs_packet_generator_0 => packet  => udp_payload_inserter

	alt_printf ("Hello Nios II - Tramas UDP OffLoad \n");

	alt_printf("------------------------\n");
	i = stop_udp_payload_inserter((void *) UDP_PAYLOAD_INSERTER_BASE);
	alt_printf("StopUDPInserter: %x \n",i);
	i = stop_packet((void *) PACKET_BASE);
	alt_printf("StopPacket: %x \n",i);
	i = stop_packet_generator((void *) GENERATOR_BASE);
	alt_printf("StopGenerator: %x \n",i);

	alt_printf("------------------------\n");
	i = start_udp_payload_inserter((void *) UDP_PAYLOAD_INSERTER_BASE, (UDP_INS_STATS *) stats);
	alt_printf("StartUDPInserter: %x \n",i);
	i = start_packet((void *) PACKET_BASE, (int) PAYLOAD_SIZE);
	alt_printf("StartPacket: %x \n",i);
	i = start_packet_generator((void *) GENERATOR_BASE);
	alt_printf("StartGenerator: %x \n",i);

	alt_printf("------------------------\n");
	i = is_udp_payload_inserter_running((void *) UDP_PAYLOAD_INSERTER_BASE);
	alt_printf("UDPInserterRunning?: %x \n",i);
	i = is_packet_running((void *) PACKET_BASE);
	alt_printf("PacketRunning?: %x \n",i);
	i = is_packet_generator_running((void *) GENERATOR_BASE);
	alt_printf("GeneratorRunning?: %x \n",i);

while (1);
return 0;
}


