//
// prbs_packet_generator
//
//  ---------------------------------------------------------------------------
//  Register Map
//  ---------------------------------------------------------------------------
//  
//  The slave interface for the prbs_packet_generator is broken up into four
//  32-bit registers with the following layout:
//  
//  Register 0 - Status Register
//      Bit 0 - R/W - GO control and status bit.  Set this bit to 1 to enable
//                  the packet generator, and clear it to disable it.  Note
//                  that once cleared, the packet generator will not truely
//                  stop until it completes the current packe that it's
//                  generating.
//      Bit 1 - RO  - Running status bit.  This bit indicates whether the
//                  peripheral is currently running or not.  After clearing the
//                  GO bit, you can monitor this status bit to tell when the
//                  generator is truely stopped.
//      
//  Register 1 - Packet Length Register
//      Bits [15:0] - R/W - byte count of packet payload length, does not
//                  include the length field of the packet. 
//                  
//  R - Readable
//  W - Writeable
//  RO - Read Only
//  WC - Clear on Write
//

module packet
(
    // clock interface
    input           csi_clock_clk,
    input           csi_clock_reset,
    
    // slave interface
    input           avs_s0_write,
    input           avs_s0_read,
    input   [1:0]   avs_s0_address,
    input   [3:0]   avs_s0_byteenable,
    input   [31:0]  avs_s0_writedata,
    output  [31:0]  avs_s0_readdata,

    // sink interface typical
    input           asi_snk0_valid,
    output          asi_snk0_ready,
    input	        asi_snk0_channel,
    input   [11:0]  asi_snk0_data,
    
    // source interface
    output          aso_src0_valid,
    input           aso_src0_ready,
    output reg [31:0]  aso_src0_data,
//  output  [1:0]   aso_src0_empty,
    output reg      aso_src0_startofpacket,
    output reg      aso_src0_endofpacket
);



localparam [4:0] IDLE_STATE     = 5'd0;
localparam [4:0] SOP_STATE      = 5'd1;
localparam [4:0] DATA_IN_STATE  = 5'd2;
localparam [4:0] DATA_OUT_STATE = 5'd3;
localparam [4:0] EOP_STATE      = 5'd4;

reg             go_bit;
reg             running_bit;
reg     [31:0]  sink_data;
reg             sink_eop;
reg     [1:0]   sink_empty;
reg     [4:0]   state;

reg             count_packet;
reg             clear_packet_count;
reg     [31:0]  packet_count;
reg     [31:0]  payload_size;
reg     [31:0]  byte_count;
reg     [16:0]  value_hi;
reg             value_hi_ok;
reg     [31:0]  next_value;

wire            pipe_src0_ready;
wire            pipe_src0_valid;
wire    [31:0]  pipe_src0_data;
wire            pipe_src0_startofpacket;
wire            pipe_src0_endofpacket;
wire    [1:0]   pipe_src0_empty;
reg     [35:0]  in_payload;
wire    [35:0]  out_payload;



always @ (posedge csi_clock_clk or posedge csi_clock_reset)
begin
 if(csi_clock_reset)
    begin
        value_hi    <= 16'b0;
        value_hi_ok <= 1'b0;
        next_value  <= 32'b0;
    end
    else
    begin
        if(asi_snk0_valid)
        begin
            if((state > SOP_STATE) && (!value_hi_ok)) 
            begin
                value_hi    <= {asi_snk0_channel, {3{1'b0}}, asi_snk0_data[11:0]};
                value_hi_ok  <= 1'b1;
            end
            else if((state > SOP_STATE) && (value_hi_ok)) 
            begin
                next_value   <= ({value_hi, asi_snk0_channel, {3{1'b0}}, asi_snk0_data[11:0]});
                value_hi_ok  <= 1'b0; 
            end                
        end
        else
        begin
            next_value  <= next_value;
            value_hi_ok <= value_hi_ok;
        end        
    end  
end

//
// packet transmit state machine
//
assign pipe_src0_data = (state == DATA_IN_STATE) ? (value_hi) :
                        (state == DATA_OUT_STATE) ? (next_value) :
                        (32'b0);

assign pipe_src0_valid =(state == IDLE_STATE)   ? (1'b0) : 
                        ((state == SOP_STATE) || (state == DATA_OUT_STATE)) ? (1'b1) : 
                        (1'b0);
                        
assign pipe_src0_startofpacket = (state == SOP_STATE) ? (1'b1) : (1'b0);

assign pipe_src0_endofpacket =  (state == EOP_STATE) ? (1'b1) : (1'b0);

assign asi_snk0_ready = (state == IDLE_STATE)      ? (1'b0) :
                        (state >= SOP_STATE) ? (go_bit & pipe_src0_ready & asi_snk0_valid) :
                        (1'b0);

always @ (posedge csi_clock_clk or posedge csi_clock_reset)
begin
    if(csi_clock_reset)
    begin
        state <= IDLE_STATE;
    end
    else
    begin
        case(state)
            IDLE_STATE:
            begin
                if(go_bit)
                begin
                    state <= SOP_STATE;
                end
            end
            SOP_STATE:
            begin
                if((payload_size < 3) && go_bit && aso_src0_valid && aso_src0_ready)
                begin
                    state <= SOP_STATE;
                end
                else if((payload_size > 6) && aso_src0_valid && aso_src0_ready)
                begin
                    if(!value_hi_ok)
                        state <= DATA_IN_STATE;
                    else
                        state <= DATA_OUT_STATE;
                end
                else if((payload_size < 7) && (payload_size > 2) && aso_src0_valid && aso_src0_ready)
                begin
                    state <= EOP_STATE;
                end
                else if((payload_size < 3) && !go_bit && aso_src0_valid && aso_src0_ready)
                begin
                    state <= IDLE_STATE;
                end
            end
            DATA_IN_STATE:
            begin
                if(((byte_count + 8) >= payload_size) && aso_src0_valid && aso_src0_ready)
                begin
                    state <= EOP_STATE;
                end
            end
            DATA_OUT_STATE:
            begin
                if(((byte_count + 8) >= payload_size) && aso_src0_valid && aso_src0_ready)
                begin
                    state <= EOP_STATE;
                end
            end
            EOP_STATE:
            begin
                if(go_bit && aso_src0_valid && aso_src0_ready)
                begin
                    state <= SOP_STATE;
                end
                else if(!go_bit && aso_src0_valid && aso_src0_ready)
                begin
                    state <= IDLE_STATE;
                end
            end
        endcase
    end
end

//
// packet_count state machine
//
always @ (posedge csi_clock_clk or posedge csi_clock_reset)
begin
    if(csi_clock_reset)
    begin
        byte_count  <= 0;
    end
    else
    begin
        case(state)
            IDLE_STATE:
            begin
                byte_count  <= 0;
            end
            SOP_STATE:
            begin
                byte_count  <= 0; //Con 4 se rompe en el largo del Datagram.
            end
            DATA_OUT_STATE:
            begin
                if(aso_src0_valid && aso_src0_ready)
                begin
                    byte_count  <= byte_count + 4;
                end
            end
            EOP_STATE:
            begin
                byte_count  <= byte_count + 4;
            end
        endcase
    end
end

//
// running_bit state machine
//
// we start immediately when go_bit is asserted
// we don't stop until we reach the end of the current packet that we're generating
//
always @ (posedge csi_clock_clk or posedge csi_clock_reset)
begin
    if(csi_clock_reset)
    begin
        running_bit <= 0;
    end
    else
    begin
        if(go_bit)
        begin
            running_bit <= 1;
        end
        else if(running_bit & !go_bit & aso_src0_valid & aso_src0_ready & aso_src0_endofpacket)
        begin
            running_bit <= 0;
        end
    end
end

//
// slave read mux
//
assign avs_s0_readdata =    (avs_s0_address == 2'h0) ?  ({{30{1'b0}}, running_bit, go_bit}) :
                            (avs_s0_address == 2'h1) ?  (payload_size) :
                                                        (32'b0);

//
// slave write demux
//
always @ (posedge csi_clock_clk or posedge csi_clock_reset)
begin
    if(csi_clock_reset)
    begin
        go_bit       <= 0;
        payload_size <= 0;
        
    end
    else
    begin
        if(avs_s0_write)
        begin
            case(avs_s0_address)
                2'h0:
                begin
                    if (avs_s0_byteenable[0] == 1'b1)
                        go_bit  <= avs_s0_writedata[0];
                end
                2'h1:
                begin
                    if (avs_s0_byteenable[0] == 1'b1)
                        payload_size <= avs_s0_writedata;
                end
            endcase
        end
        else
        begin
           go_bit       <= go_bit;
           payload_size <= payload_size;
        end
    end
end

//
// output Pipeline
//
udp_payload_inserter_1stage_pipeline #( .PAYLOAD_WIDTH( 36 ) ) outpipe (
    .clk            (csi_clock_clk ),
    .reset_n        (csi_clock_reset),
    .in_ready       (pipe_src0_ready),
    .in_valid       (pipe_src0_valid), 
    .in_payload     (in_payload),
    .out_ready      (aso_src0_ready), 
    .out_valid      (aso_src0_valid), 
    .out_payload    (out_payload)
);

//
// Output Mapping
//
always @* begin
    in_payload <= {pipe_src0_data, asi_snk0_channel};
    {aso_src0_data, aso_src0_startofpacket, aso_src0_endofpacket} <= out_payload;
end

endmodule

//  -------------------------------------------------------------------------------
// | single buffered pipeline stage                            |
//  -------------------------------------------------------------------------------
module udp_payload_inserter_1stage_pipeline  
#( parameter PAYLOAD_WIDTH = 12 )
 ( input                          clk,
   input                          reset_n, 
   output reg                     in_ready,
   input                          in_valid,   
   input      [PAYLOAD_WIDTH-1:0] in_payload,
   input                          out_ready,   
   output reg                     out_valid,
   output reg [PAYLOAD_WIDTH-1:0] out_payload      
 );
      
   always @* begin
     in_ready <= out_ready || ~out_valid;
   end
   
   always @ (posedge reset_n, posedge clk) begin
      if (reset_n) begin
         out_valid <= 0;
         out_payload <= 0;
      end else begin
         if (in_valid) begin
           out_valid <= 1;
         end else if (out_ready) begin
           out_valid <= 0;
         end
         
         if(in_valid && in_ready) begin
            out_payload <= in_payload;
         end
      end
   end

endmodule