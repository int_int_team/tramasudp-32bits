#ifndef PACKET_EVB_REGS_H
#define PACKET_EVB_REGS_H

#include "io.h"

// packet_evb accessor macros

#define PACKET_EVB_RD_CSR(base)                  IORD(base, 0)
#define PACKET_EVB_WR_CSR(base, data)            IOWR(base, 0, data)

#define PACKET_EVB_CSR_GO_BIT_MASK               (0x01)
#define PACKET_EVB_CSR_GO_BIT_OFST               (0)
#define PACKET_EVB_CSR_RUNNING_BIT_MASK          (0x02)
#define PACKET_EVB_CSR_RUNNING_BIT_OFST          (1)

#define PACKET_EVB_RD_BYTE_COUNT(base)           IORD(base, 1)
#define PACKET_EVB_BYTE_COUNT(base, data)     	 IOWR(base, 1, data)

#define PACKET_EVB_BYTE_COUNT_MASK               (0xFFFF)
#define PACKET_EVB_BYTE_COUNT_OFST               (0)

#define PACKET_EVB_RD_INITIAL_VALUE(base)        IORD(base, 2)
#define PACKET_EVB_WR_INITIAL_VALUE(base, data)  IOWR(base, 2, data)

#define PACKET_EVB_INITIAL_VALUE_MASK            (0xFFFFFFFF)
#define PACKET_EVB_INITIAL_VALUE_OFST            (0)

#define PACKET_EVB_RD_PACKET_COUNTER(base)       IORD(base, 3)
#define PACKET_EVB_CLEAR_PACKET_COUNTER(base)    IOWR(base, 3, 0)

#define PACKET_EVB_PACKET_COUNTER_MASK           (0xFFFFFFFF)
#define PACKET_EVB_PACKET_COUNTER_OFST           (0)

#endif /*PRBS_PACKET_EVB_REGS_H*/
