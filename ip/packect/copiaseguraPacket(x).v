//
// prbs_packet_generator
//
//  ---------------------------------------------------------------------------
//  Register Map
//  ---------------------------------------------------------------------------
//  
//  The slave interface for the prbs_packet_generator is broken up into four
//  32-bit registers with the following layout:
//  
//  Register 0 - Status Register
//      Bit 0 - R/W - GO control and status bit.  Set this bit to 1 to enable
//                  the packet generator, and clear it to disable it.  Note
//                  that once cleared, the packet generator will not truely
//                  stop until it completes the current packe that it's
//                  generating.
//      Bit 1 - RO  - Running status bit.  This bit indicates whether the
//                  peripheral is currently running or not.  After clearing the
//                  GO bit, you can monitor this status bit to tell when the
//                  generator is truely stopped.
//      
//  Register 1 - Packet Length Register
//      Bits [15:0] - R/W - byte count of packet payload length, does not
//                  include the length field of the packet. 
//                  
//  R - Readable
//  W - Writeable
//  RO - Read Only
//  WC - Clear on Write
//

module packet
(
    // clock interface
    input           csi_clock_clk,
    input           csi_clock_reset,
    
    // slave interface
    input           avs_s0_write,
    input           avs_s0_read,
    input   [1:0]   avs_s0_address,
    input   [3:0]   avs_s0_byteenable,
    input   [31:0]  avs_s0_writedata,
    output  [31:0]  avs_s0_readdata,

    // sink interface typical
    input           asi_snk0_valid,
    output          asi_snk0_ready,
    input	        asi_snk0_channel,
    input   [11:0]  asi_snk0_data,
    
    // source interface
    output          aso_src0_valid,
    input           aso_src0_ready,
    output  [31:0]  aso_src0_data,
//  output  [1:0]   aso_src0_empty,
    output          aso_src0_startofpacket,
    output          aso_src0_endofpacket
);

localparam [1:0] IDLE_STATE = 2'h0;
localparam [1:0] SOP_STATE  = 2'h1;
localparam [1:0] DATA_STATE = 2'h2;
localparam [1:0] EOP_STATE  = 2'h3;

reg             go_bit;
reg             running_bit;
reg     [31:0]  payload_size;
reg     [15:0]  byte_count;
reg     [31:0]  next_value;
reg     [15:0]  value_hi;
reg             value_valid;
reg             value_hi_ok;
reg     [1:0]   state;
reg             ready_sink;
reg             value_sink;
wire            ready_sink_wire;

//
// slave read mux
//
assign avs_s0_readdata =    (avs_s0_address == 2'h0) ?  ({{30{1'b0}}, running_bit, go_bit}) :
                            (avs_s0_address == 2'h1) ?  (payload_size) :
                                                        (32'b0);

//
// slave write demux
//
always @ (posedge csi_clock_clk or posedge csi_clock_reset)
begin
    if(csi_clock_reset)
    begin
        go_bit       <= 0;
        payload_size <= 0;
        
    end
    else
    begin
        if(avs_s0_write)
        begin
            case(avs_s0_address)
                2'h0:
                begin
                    if (avs_s0_byteenable[0] == 1'b1)
                        go_bit  <= avs_s0_writedata[0];
                end
                2'h1:
                begin
                    if (avs_s0_byteenable[0] == 1'b1)
                        payload_size <= avs_s0_writedata;
                end
            endcase
        end
        else
        begin
           go_bit 		<= go_bit;
           payload_size <= payload_size;
        end
    end
end

//
// running_bit state machine
//
// we start immediately when go_bit is asserted
// we don't stop until we reach the end of the current packet that we're generating
//
always @ (posedge csi_clock_clk or posedge csi_clock_reset)
begin
    if(csi_clock_reset)
    begin
        running_bit <= 0;
    end
    else
    begin
        if(go_bit)
        begin
            running_bit <= 1;
        end
        else if(running_bit & !go_bit & aso_src0_valid & aso_src0_ready & aso_src0_endofpacket)
        begin
            running_bit <= 0;
        end
    end
end

//
// byte_count state machine
//
// this state machine counts the number of bytes that have been transmitted in
// current packet.
//

always @ (posedge csi_clock_clk or posedge csi_clock_reset)
begin
    if(csi_clock_reset)
    begin
        byte_count  <= 0;
    end
    else
    begin
        case(state)
            IDLE_STATE:
            begin
                byte_count  <= 0;
            end
            SOP_STATE:
            begin
                byte_count  <= 0; //Con 4 se rompe en el largo del Datagram.
            end
            DATA_STATE:
            begin
                if(aso_src0_valid && aso_src0_ready)
                begin
                    byte_count  <= byte_count + 4;
                end
            end
            EOP_STATE:
            begin
                byte_count  <= byte_count + 4;
            end
        endcase
    end
end

//
// sink & next_value state machine
//
always @ (posedge csi_clock_clk or posedge csi_clock_reset)
begin
 if(csi_clock_reset)
    begin
        value_hi    <= 16'b0;
        value_hi_ok <= 1'b0;
        value_sink  <= 1'b0;
        next_value  <= 32'b0;
    end
    else
    begin
        if(running_bit)
        begin
            if (asi_snk0_valid && !value_hi_ok) 
            begin
                next_value[31:16] <= ({asi_snk0_channel, {3{1'b0}}, asi_snk0_data[11:0]});
                value_hi_ok <= 1'b1;
                value_valid <= 1'b0;
                ready_sink  <= 1'b1;
            end
            if (asi_snk0_valid && value_hi_ok)
            begin
                next_value[15:0] <= ({asi_snk0_channel, {3{1'b0}}, asi_snk0_data[11:0]});
                value_hi_ok  <= 1'b0; 
                value_valid  <= 1'b1;
                ready_sink   <= 1'b1;
            end                
        end
        else
        begin
            next_value  <= next_value;
            value_hi_ok <= value_hi_ok;
            value_valid <= 1'b0;
            ready_sink  <= 1'b0;
        end        
    end  
end

//assign asi_snk0_ready = ready_sink;    //Anda pero se salta 5 datos cada 4 secuenciales
/*
assign ready_sink_wire = ready_sink;     //-------- Anda pero se salta 5 datos cada 4 secuenciales.

always @ (*)
begin  
    if (aso_src0_ready || ready_sink_wire)
    begin
            value_sink <= 1;
    end        
    else
    begin
            value_sink <= 0;
    end
end
assign asi_snk0_ready = value_sink;     //--------------------------------------------------------------------
*/


//
// ready Sink Interface
//  
//assign asi_snk0_ready = ((aso_src0_ready == 1'b1) || (ready_sink == 1'b1)) ? 1'b1 : 1'b0; //Anda pero se salta 5 datos cada 4 secuenciales.

//assign asi_snk0_ready = aso_src0_ready; //Secuencia pero se repiten valores 

assign asi_snk0_ready = ready_sink || aso_src0_ready; //Anda pero se salta 5 datos cada 4 secuenciales.
//que pasa si el ready src esta bajo y el valid src en uno? significa que el ready sink debe estar en uno, por lo tanto el generetor suma
//assign asi_snk0_ready = ready_sink & aso_src0_ready; //Problemas con el printf 

//
// source interface control
//

assign aso_src0_valid           =   value_valid;
									
assign aso_src0_data            =   (state == SOP_STATE) ? (payload_size) : (next_value);

assign aso_src0_startofpacket   =   (state == SOP_STATE) ? (1'b1) : (1'b0);

assign aso_src0_endofpacket     =   (state == EOP_STATE) ? (1'b1) : 
                                    ((state == SOP_STATE) && (payload_size < 3)) ? (1'b1) : (1'b0);

//
// source state machine
//
// this state machine provides synchronous sequencing for the control of the
// source interface
//
always @ (posedge csi_clock_clk or posedge csi_clock_reset)
begin
    if(csi_clock_reset)
    begin
        state <= IDLE_STATE;
    end
    else
    begin
        case(state)
            IDLE_STATE:
            begin
                if(go_bit)
                begin
                    state <= SOP_STATE;
                end
            end
            SOP_STATE:
            begin
                if((payload_size < 3) && go_bit && aso_src0_valid && aso_src0_ready)
                begin
                    state <= SOP_STATE;
                end
                else if((payload_size > 6) && aso_src0_valid && aso_src0_ready)
                begin
                    state <= DATA_STATE;
                end
                else if((payload_size < 7) && (payload_size > 2) && aso_src0_valid && aso_src0_ready)
                begin
                    state <= EOP_STATE;
                end
                else if((payload_size < 3) && !go_bit && aso_src0_valid && aso_src0_ready)
                begin
                    state <= IDLE_STATE;
                end
            end
            DATA_STATE:
            begin
                if(((byte_count + 8) >= payload_size) && aso_src0_valid && aso_src0_ready)
                begin
                    state <= EOP_STATE;
                end
            end
            EOP_STATE:
            begin
                if(go_bit && aso_src0_valid && aso_src0_ready)
                begin
                    state <= SOP_STATE;
                end
                else if(!go_bit && aso_src0_valid && aso_src0_ready)
                begin
                    state <= IDLE_STATE;
                end
            end
        endcase
    end
end
endmodule