# TCL File Generated by Component Editor 16.0
# Thu May 11 17:51:15 ART 2017
# DO NOT MODIFY


# 
# udp_payload_inserter "udp_payload_inserter" v1.0
# RSF 2017.05.11.17:51:15
# This component inserts RAW data into a UDP packet for Ethernet transmission.
# 

# 
# request TCL package from ACDS 16.0
# 
package require -exact qsys 16.0


# 
# module udp_payload_inserter
# 
set_module_property DESCRIPTION "This component inserts RAW data into a UDP packet for Ethernet transmission."
set_module_property NAME udp_payload_inserter
set_module_property VERSION 1.0
set_module_property INTERNAL false
set_module_property OPAQUE_ADDRESS_MAP true
set_module_property GROUP "Packet Processing"
set_module_property AUTHOR RSF
set_module_property DISPLAY_NAME udp_payload_inserter
set_module_property INSTANTIATE_IN_SYSTEM_MODULE true
set_module_property EDITABLE true
set_module_property REPORT_TO_TALKBACK false
set_module_property ALLOW_GREYBOX_GENERATION false
set_module_property REPORT_HIERARCHY false


# 
# file sets
# 
add_fileset quartus_synth QUARTUS_SYNTH "" "Quartus Synthesis"
set_fileset_property quartus_synth TOP_LEVEL udp_payload_inserter
set_fileset_property quartus_synth ENABLE_RELATIVE_INCLUDE_PATHS false
set_fileset_property quartus_synth ENABLE_FILE_OVERWRITE_MODE false
add_fileset_file udp_payload_inserter.v VERILOG PATH udp_payload_inserter.v TOP_LEVEL_FILE

add_fileset sim_verilog SIM_VERILOG "" "Verilog Simulation"
set_fileset_property sim_verilog TOP_LEVEL udp_payload_inserter
set_fileset_property sim_verilog ENABLE_RELATIVE_INCLUDE_PATHS false
set_fileset_property sim_verilog ENABLE_FILE_OVERWRITE_MODE false
add_fileset_file udp_payload_inserter.v VERILOG PATH udp_payload_inserter.v


# 
# parameters
# 


# 
# display items
# 


# 
# connection point clock
# 
add_interface clock clock end
set_interface_property clock clockRate 0
set_interface_property clock ENABLED true
set_interface_property clock EXPORT_OF ""
set_interface_property clock PORT_NAME_MAP ""
set_interface_property clock CMSIS_SVD_VARIABLES ""
set_interface_property clock SVD_ADDRESS_GROUP ""

add_interface_port clock csi_clock_clk clk Input 1


# 
# connection point clock_reset
# 
add_interface clock_reset reset end
set_interface_property clock_reset associatedClock clock
set_interface_property clock_reset synchronousEdges DEASSERT
set_interface_property clock_reset ENABLED true
set_interface_property clock_reset EXPORT_OF ""
set_interface_property clock_reset PORT_NAME_MAP ""
set_interface_property clock_reset CMSIS_SVD_VARIABLES ""
set_interface_property clock_reset SVD_ADDRESS_GROUP ""

add_interface_port clock_reset csi_clock_reset reset Input 1


# 
# connection point s0
# 
add_interface s0 avalon end
set_interface_property s0 addressUnits WORDS
set_interface_property s0 associatedClock clock
set_interface_property s0 associatedReset clock_reset
set_interface_property s0 bitsPerSymbol 8
set_interface_property s0 burstOnBurstBoundariesOnly false
set_interface_property s0 burstcountUnits WORDS
set_interface_property s0 explicitAddressSpan 0
set_interface_property s0 holdTime 0
set_interface_property s0 linewrapBursts false
set_interface_property s0 maximumPendingReadTransactions 0
set_interface_property s0 maximumPendingWriteTransactions 0
set_interface_property s0 readLatency 0
set_interface_property s0 readWaitTime 1
set_interface_property s0 setupTime 0
set_interface_property s0 timingUnits Cycles
set_interface_property s0 writeWaitTime 0
set_interface_property s0 ENABLED true
set_interface_property s0 EXPORT_OF ""
set_interface_property s0 PORT_NAME_MAP ""
set_interface_property s0 CMSIS_SVD_VARIABLES ""
set_interface_property s0 SVD_ADDRESS_GROUP ""

add_interface_port s0 avs_s0_write write Input 1
add_interface_port s0 avs_s0_read read Input 1
add_interface_port s0 avs_s0_address address Input 4
add_interface_port s0 avs_s0_byteenable byteenable Input 4
add_interface_port s0 avs_s0_writedata writedata Input 32
add_interface_port s0 avs_s0_readdata readdata Output 32
set_interface_assignment s0 embeddedsw.configuration.isFlash 0
set_interface_assignment s0 embeddedsw.configuration.isMemoryDevice false
set_interface_assignment s0 embeddedsw.configuration.isNonVolatileStorage false
set_interface_assignment s0 embeddedsw.configuration.isPrintableDevice false


# 
# connection point src0
# 
add_interface src0 avalon_streaming start
set_interface_property src0 associatedClock clock
set_interface_property src0 associatedReset clock_reset
set_interface_property src0 dataBitsPerSymbol 8
set_interface_property src0 errorDescriptor ""
set_interface_property src0 firstSymbolInHighOrderBits true
set_interface_property src0 maxChannel 0
set_interface_property src0 readyLatency 0
set_interface_property src0 ENABLED true
set_interface_property src0 EXPORT_OF ""
set_interface_property src0 PORT_NAME_MAP ""
set_interface_property src0 CMSIS_SVD_VARIABLES ""
set_interface_property src0 SVD_ADDRESS_GROUP ""

add_interface_port src0 aso_src0_valid valid Output 1
add_interface_port src0 aso_src0_ready ready Input 1
add_interface_port src0 aso_src0_data data Output 32
add_interface_port src0 aso_src0_empty empty Output 2
add_interface_port src0 aso_src0_startofpacket startofpacket Output 1
add_interface_port src0 aso_src0_endofpacket endofpacket Output 1
add_interface_port src0 aso_src0_error error Output 1


# 
# connection point snk0
# 
add_interface snk0 avalon_streaming end
set_interface_property snk0 associatedClock clock
set_interface_property snk0 associatedReset clock_reset
set_interface_property snk0 dataBitsPerSymbol 32
set_interface_property snk0 errorDescriptor ""
set_interface_property snk0 firstSymbolInHighOrderBits true
set_interface_property snk0 maxChannel 0
set_interface_property snk0 readyLatency 0
set_interface_property snk0 symbolsPerBeat 4
set_interface_property snk0 ENABLED true
set_interface_property snk0 EXPORT_OF ""
set_interface_property snk0 PORT_NAME_MAP ""
set_interface_property snk0 CMSIS_SVD_VARIABLES ""
set_interface_property snk0 SVD_ADDRESS_GROUP ""

add_interface_port snk0 asi_snk0_valid valid Input 1
add_interface_port snk0 asi_snk0_ready ready Output 1
add_interface_port snk0 asi_snk0_data data Input 32
add_interface_port snk0 asi_snk0_startofpacket startofpacket Input 1
add_interface_port snk0 asi_snk0_endofpacket endofpacket Input 1

