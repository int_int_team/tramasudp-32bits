#ifndef EVB_INTERFACE_REGS_H
#define EVB_INTERFACE_REGS_H

#include "io.h"

// EVB_INTERFACE_REGS accessor macros

#define EVB_INTERFACE_RD_CSR(base)                  IORD(base, 0)
#define EVB_INTERFACE_WR_CSR(base, data)            IOWR(base, 0, data)

#define EVB_INTERFACE_CSR_GO_BIT_MASK               (0x01)
#define EVB_INTERFACE_CSR_GO_BIT_OFST               (0)
#endif /*EVB_INTERFACE_REGS_H*/
