transcript on
if {[file exists rtl_work]} {
	vdel -lib rtl_work -all
}
vlib rtl_work
vmap work rtl_work

vlog -vlog01compat -work work +incdir+/home/maxi/Descargas/tesis/tramas-32bits {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tse.v}
vlog -vlog01compat -work work +incdir+/home/maxi/Descargas/tesis/tramas-32bits {/home/maxi/Descargas/tesis/tramas-32bits/pll.v}
vlog -vlog01compat -work work +incdir+/home/maxi/Descargas/tesis/tramas-32bits/db {/home/maxi/Descargas/tesis/tramas-32bits/db/pll_altpll.v}
vlib niosf_tx_tse_125mhz
vmap niosf_tx_tse_125mhz niosf_tx_tse_125mhz
vlog -vlog01compat -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/niosf_tx_tse_125mhz.v}
vlog -vlog01compat -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/altera_reset_controller.v}
vlog -vlog01compat -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/altera_reset_synchronizer.v}
vlog -vlog01compat -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/niosf_tx_tse_125mhz_mm_interconnect_0.v}
vlog -vlog01compat -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/niosf_tx_tse_125mhz_mm_interconnect_0_avalon_st_adapter.v}
vlog -vlog01compat -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/udp_payload_inserter.v}
vlog -vlog01compat -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/niosf_tx_tse_125mhz_tse.v}
vlog -vlog01compat -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/packet.v}
vlog -vlog01compat -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/niosf_tx_tse_125mhz_niosfast.v}
vlog -vlog01compat -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/niosf_tx_tse_125mhz_mm.v}
vlog -vlog01compat -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/niosf_tx_tse_125mhz_jtag_uart.v}
vlog -vlog01compat -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/generator.v}
vlog -sv -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/niosf_tx_tse_125mhz_irq_mapper.sv}
vlog -sv -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/niosf_tx_tse_125mhz_mm_interconnect_0_avalon_st_adapter_error_adapter_0.sv}
vlog -sv -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/niosf_tx_tse_125mhz_mm_interconnect_0_rsp_mux_001.sv}
vlog -sv -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/altera_merlin_arbitrator.sv}
vlog -sv -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/niosf_tx_tse_125mhz_mm_interconnect_0_rsp_mux.sv}
vlog -sv -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/niosf_tx_tse_125mhz_mm_interconnect_0_rsp_demux_001.sv}
vlog -sv -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/niosf_tx_tse_125mhz_mm_interconnect_0_rsp_demux.sv}
vlog -sv -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/niosf_tx_tse_125mhz_mm_interconnect_0_cmd_mux_001.sv}
vlog -sv -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/niosf_tx_tse_125mhz_mm_interconnect_0_cmd_mux.sv}
vlog -sv -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/niosf_tx_tse_125mhz_mm_interconnect_0_cmd_demux_001.sv}
vlog -sv -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/niosf_tx_tse_125mhz_mm_interconnect_0_cmd_demux.sv}
vlog -sv -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/altera_merlin_traffic_limiter.sv}
vlog -vlog01compat -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/altera_avalon_sc_fifo.v}
vlog -sv -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/niosf_tx_tse_125mhz_mm_interconnect_0_router_003.sv}
vlog -sv -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/niosf_tx_tse_125mhz_mm_interconnect_0_router_002.sv}
vlog -sv -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/niosf_tx_tse_125mhz_mm_interconnect_0_router_001.sv}
vlog -sv -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/niosf_tx_tse_125mhz_mm_interconnect_0_router.sv}
vlog -sv -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/altera_merlin_slave_agent.sv}
vlog -sv -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/altera_merlin_burst_uncompressor.sv}
vlog -sv -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/altera_merlin_master_agent.sv}
vlog -sv -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/altera_merlin_slave_translator.sv}
vlog -sv -work niosf_tx_tse_125mhz +incdir+/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules {/home/maxi/Descargas/tesis/tramas-32bits/niosf_tx_tse_125mhz/synthesis/submodules/altera_merlin_master_translator.sv}

